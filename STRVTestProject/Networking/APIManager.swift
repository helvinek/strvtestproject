//
//  APIManager.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 2/27/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import ObjectMapper

class APIManager : NSObject {
    
    let kAPIkey = "9afcefd5e22adba2c7e4b69db7b30b87"
    let kMetricUnit = "metric"
    let kAppIDKey = "appid"
    let kUnitsKey = "units"
    let kLangKey = "lang"
    
    let baseURL = "http://api.openweathermap.org/data/2.5/"
    let weatherMethod = "weather"
    let forecastMethod = "forecast"
    
    static let shared = APIManager()
    
    override private init () {}
    
    private func prepareParams (_ params : [String : Any]) -> [String : Any] {
        
        let language = Locale.preferredLanguages.first ?? "en"
        var requestParams = params
        requestParams[kAppIDKey] = kAPIkey
        requestParams[kUnitsKey] = kMetricUnit
        requestParams[kLangKey] = language
        return requestParams
    }
    
    func fetchCurrentWeatherObservable(latitude : String , longitude : String) -> Observable<CurrentWeather> {
        
        let url = self.baseURL + self.weatherMethod
        let params = ["lat" : latitude,
                      "lon" : longitude]
        let requestParams = self.prepareParams(params)
        return Observable<CurrentWeather>.create { observer in
            
            AF.request(url, parameters : requestParams)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    guard response.result.isSuccess else {
                        observer.onError(response.error!)
                        return
                    }
                    if let json = response.result.value{
                        if let currentWeather = Mapper<CurrentWeather>().map(JSONObject: json){
                            observer.onNext(currentWeather)
                        } else {
                            let error = NSError(domain: "Error parsing data", code: -999, userInfo: nil)
                            observer.onError(error)
                        }
                    }
                    
                    observer.onCompleted()
                })
            
            return Disposables.create()
        }
    }
    
    func fetchForecastObservable(latitude : String , longitude : String) -> Observable<[Forecast]> {
        
        let url = self.baseURL + self.forecastMethod
        let params = ["lat" : latitude,
                      "lon" : longitude]
        let requestParams = self.prepareParams(params)
        return Observable<[Forecast]>.create { observer in
            
            AF.request(url, parameters : requestParams)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    guard response.result.isSuccess else {
                        observer.onError(response.error!)
                        return
                    }
                    
                    if let json = response.result.value as? [String : Any]{
                        if let arrayDict = json["list"] as? [[String : Any]]{
                            if let forecastArray = Mapper<Forecast>().mapArray(JSONObject: arrayDict),
                                   forecastArray.count > 0{
                                observer.onNext(forecastArray)
                            } else {
                                let error = NSError(domain: "Error parsing data", code: -999, userInfo: nil)
                                observer.onError(error)
                            }
                        }
                        
                    }
                    observer.onCompleted()
                })
            
            return Disposables.create()
        }
    }
}
