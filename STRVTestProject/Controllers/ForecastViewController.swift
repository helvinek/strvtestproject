//
//  ForecastViewController.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation

class ForecastViewController: UIViewController {

    @IBOutlet var forecastTableView : UITableView!
    @IBOutlet var mainContainerView : UIView!
    @IBOutlet var errorMessageLabel : UILabel!
    
    var disposeBag = DisposeBag()
    var viewModel = ForecastViewModel()
    
    let kForecastCellNibName = "ForecastTableViewCell"
    let kForecastCellIdentifier = "forecastCell"
    let kHeaderCellIdentifier = "headerCell"
    
    let dataSource = RxTableViewSectionedReloadDataSource<SectionOfForecast>(
        configureCell: { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastTableViewCell
            cell.configureCellWithForecast(item, showSeparator: true)
            return cell
    })
    
    override func viewDidLoad() {

        self.errorMessageLabel.isHidden = true
        self.forecastTableView.isHidden = true
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: 16)!]

        
        setupDataSource()
        
        if let nav = self.navigationController{
            if let tabBar = nav.parent as? TabBarViewController {
                tabBar.location.subscribe(
                    onNext: { [weak self] location in
                        if let location = location {
                            if let self = self {
                                self.viewModel
                                    .refreshForecastWithLocation(lat: "\(location.coordinate.latitude)" ,
                                        lon: "\(location.coordinate.longitude)")
                            }
                        }
                }).disposed(by: disposeBag)
            }
        }
        viewModel.errorMessage.subscribe(onNext:{[unowned self] message in
            if let message = message {
                self.errorMessageLabel.text = message
                self.errorMessageLabel.isHidden = false
                self.forecastTableView.isHidden = true
            } else {
                self.errorMessageLabel.isHidden = true
                self.forecastTableView.isHidden = false
            }
            
        }).disposed(by: disposeBag)
    }
    
    private func setupDataSource(){
        
        forecastTableView.register(UINib(nibName: kForecastCellNibName, bundle: nil), forCellReuseIdentifier: kForecastCellIdentifier)
        forecastTableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        let dataSource = self.dataSource
    
        viewModel.orderedForecasts
            .bind(to: forecastTableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
    }
}


extension ForecastViewController : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: kHeaderCellIdentifier) as! ForecastHeaderTableViewCell
        let sectionModel = self.dataSource.sectionModels[section]
        let forecast = sectionModel.items[0]
        cell.configCellWithForecast(forecast, isToday: section == 0)
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}
