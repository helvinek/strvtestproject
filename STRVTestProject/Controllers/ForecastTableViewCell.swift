//
//  ForecastTableViewCell.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 2/28/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
    
    @IBOutlet var forecastImageView: UIImageView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var forecastDescriptionLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var separatorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Config methods
    func configureCellWithForecast (_ forecast : Forecast, showSeparator : Bool){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        forecastImageView.image = UIImage(named: CurrentWeather.getImageNameWithCode(forecast.icon))
        timeLabel.text = dateFormatter.string(from: forecast.date)
        temperatureLabel.text = "\(Int(forecast.temperature))°"
        forecastDescriptionLabel.text = forecast.weatherDescription.capitalized
        separatorView.isHidden = !showSeparator
        
    }
    
}
