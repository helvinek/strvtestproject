//
//  CurrentWeatherViewController.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreLocation

class CurrentWeatherViewController: UIViewController {
    @IBOutlet var weatherImageView: UIImageView!
    @IBOutlet var locationImage: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var weatherLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var precipitationLabel: UILabel!
    @IBOutlet var pressureLabel: UILabel!
    @IBOutlet var windSpeedLabel: UILabel!
    @IBOutlet var windDirectionLabel: UILabel!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var mainContainerView : UIView!
    @IBOutlet var errorMessageLabel : UILabel!
    
    var viewModel = CurrentWeatherViewModel()
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorMessageLabel.isHidden = true
        self.mainContainerView.isHidden = true
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: 16)!]
        bindViews()
    }
    
    private func bindViews () {
        viewModel.weatherImage.bind(to: weatherImageView.rx.image).disposed(by: disposeBag)
        viewModel.formattedLocation.bind(to: locationLabel.rx.text).disposed(by: disposeBag)
        viewModel.formattedWeatherInfo.bind(to: weatherLabel.rx.text).disposed(by: disposeBag)
        viewModel.humidity.bind(to: humidityLabel.rx.text).disposed(by: disposeBag)
        viewModel.precipitation.bind(to: precipitationLabel.rx.text).disposed(by: disposeBag)
        viewModel.pressure.bind(to: pressureLabel.rx.text).disposed(by: disposeBag)
        viewModel.windSpeed.bind(to: windSpeedLabel.rx.text).disposed(by: disposeBag)
        viewModel.windDirection.bind(to: windDirectionLabel.rx.text).disposed(by: disposeBag)
        
        viewModel.errorMessage.subscribe(onNext:{[unowned self] message in
            if let message = message {
                self.errorMessageLabel.text = message
                self.errorMessageLabel.isHidden = false
                self.mainContainerView.isHidden = true
            } else {
                self.errorMessageLabel.isHidden = true
                self.mainContainerView.isHidden = false
            }
            
        }).disposed(by: disposeBag)
        
        shareButton.rx
                   .tap
                   .subscribe(onNext : {[weak self] button in
                        if let self = self {
                            let currentWeatherImage = self.view.asImage()
                            self.presentActivityController(sender : self.shareButton , items : [currentWeatherImage])
                        }
                   })
                   .disposed(by: disposeBag)
        
        
        
        
        if let nav = self.navigationController{
            if let tabBar = nav.parent as? TabBarViewController {
                tabBar.location.subscribe(
                    onNext: { [weak self] location in
                        if let location = location {
                            if let self = self {
                                self.viewModel
                                    .refreshCurrentWeatherWithLocation(lat: "\(location.coordinate.latitude)" ,
                                        lon: "\(location.coordinate.longitude)")
                            }
                        }
                }).disposed(by: disposeBag)
            }
        }
    }
    //MARK: - Private methods
    private func presentActivityController ( sender: UIButton,  items : [Any]){
        DispatchQueue.main.async {
            let activity = UIActivityViewController(activityItems: items, applicationActivities: nil)
            if UIDevice.current.userInterfaceIdiom == .pad {
                activity.popoverPresentationController?.sourceView = sender
                activity.popoverPresentationController?.sourceRect = CGRect(x: sender.bounds.midX, y: 0, width: 0, height: 0)
            }
            self.present(activity, animated: true, completion: nil)
        }
    }
}

