//
//  TabBarViewController.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa

class TabBarViewController: UITabBarController {
    
    private var locationManager : CLLocationManager?
    var location = BehaviorRelay<CLLocation?>(value: nil)
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if CLLocationManager.locationServicesEnabled(){
            locationManager?.requestLocation()
        } else {
            locationManager?.requestWhenInUseAuthorization()
        }
    }
    
    //MARK: - Private methods
    
    private func showLocationSettingsAlert(){
        
        let alertController = UIAlertController(
            title: NSLocalizedString("locationAccessDisabled", comment: ""),
            message: NSLocalizedString("locationAccessMessage", comment: ""),
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: NSLocalizedString("openSettings", comment: ""), style: .default) { (action) in
            if let url = URL(string:UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showLocationNotFoundAlert(){
        let alert = UIAlertController(title: NSLocalizedString("warning", comment: ""),
                                      message: NSLocalizedString("locationErrorMessage", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension TabBarViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.location.accept(location)
        } else {
            showLocationNotFoundAlert()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showLocationNotFoundAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied:
            showLocationSettingsAlert()
        case .authorizedAlways, .authorizedWhenInUse:
            if CLLocationManager.locationServicesEnabled() {
                locationManager?.requestLocation()
            }
        default:
            locationManager?.requestWhenInUseAuthorization()
        }
    }
}
