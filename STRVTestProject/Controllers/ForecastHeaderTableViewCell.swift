//
//  ForecastHeaderTableViewCell.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 2/28/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import UIKit

class ForecastHeaderTableViewCell: UITableViewCell {

    @IBOutlet var dayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Config methods
    func configCellWithForecast (_ forecast : Forecast, isToday : Bool){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dayLabel.text = isToday ? NSLocalizedString("today", comment: "").uppercased() : dateFormatter.string(from: forecast.date).uppercased()
        
    }

}
