//
//  CurrentWeather.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import Foundation
import ObjectMapper


struct CurrentWeather : Mappable {
    
    var cityName : String?
    var countryCode : String?
    var imageName : String?
    var weatherDescription : String?
    var latitude : Float?
    var longitude : Float?
    var temperature : Float?
    var pressure : Float?
    var humidity : Float?
    var precipitation : Float?
    var windSpeed : Float?
    var windDegrees : Float?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        cityName <- map["name"]
        countryCode <- map["sys.country"]
        imageName <- map["weather.0.icon"]
        weatherDescription <- map["weather.0.description"]
        latitude <- map["coord.latitude"]
        longitude <- map["coord.longitude"]
        temperature <- map["main.temp"]
        pressure <- map["main.pressure"]
        humidity <- map["main.humidity"]
        precipitation <- map["rain.1h"]
        windSpeed <- map["wind.speed"]
        windDegrees <- map["wind.deg"]
    }
    
    
    init(dictionary : [String : Any]) {
        self.cityName = dictionary["name"] as? String ?? ""
        if let sys = dictionary["sys"] as? [String : Any] {
            self.countryCode = sys["country"] as? String ?? ""
        } else {
            self.countryCode = ""
        }
        
        if let coord = dictionary["coord"] as? [String : Any] {
            self.latitude = coord["latitude"] as? Float ?? 0
            self.longitude = coord["longitude"] as? Float ?? 0
        } else {
            self.latitude = 0
            self.longitude = 0
        }
        
        if let indicators = dictionary["main"] as? [String : Any] {
            self.temperature = indicators["temp"] as? Float ?? 0
            self.pressure = indicators["pressure"] as? Float ?? 0
            self.humidity = indicators["humidity"] as? Float ?? 0
        } else {
            
            self.temperature = 0
            self.pressure = 0
            self.humidity = 0
        }
        
        if let weather = dictionary["weather"] as? [String : Any] {
            self.weatherDescription = weather["description"] as? String ?? ""
            self.imageName = CurrentWeather.getImageNameWithCode (weather["icon"] as? String)
        } else {
            self.weatherDescription = ""
            self.imageName = ""
        }
        
        if let wind = dictionary["wind"] as? [String : Any] {
            self.windSpeed = wind["speed"] as? Float ?? 0
            self.windDegrees = wind["deg"] as? Float ?? 0
        } else {
            self.windSpeed = 0
            self.windDegrees = 0
        }
        
        if let rain = dictionary["rain"] as? [String : Any] {
            self.precipitation = rain["1h"] as? Float ?? rain["3h"] as? Float ?? 0
        } else {
            self.precipitation = 0
        }
    }
    
    
    static func getImageNameWithCode(_ code : String?) -> String {
        guard let code = code else {
            return ""
        }
        
        let weatherImageDictionary = ["01d" : "ClearSkyDay",
                                      "01n" : "ClearSkyNight",
                                      "02d" : "FewCloudsDay",
                                      "02n" : "FewCloudsNight",
                                      "03d" : "ScatteredCloudsDay",
                                      "03n" : "ScatteredCloudsNight",
                                      "04d" : "BrokenCloudsDay",
                                      "04n" : "BrokenCloudsNight",
                                      "09d" : "ShowerRainDay",
                                      "09n" : "ShowerRainNight",
                                      "10d" : "RainDay",
                                      "10n" : "RainNight",
                                      "11d" : "ThunderstormDay",
                                      "11n" : "ThunderstormNight",
                                      "13d" : "SnowDay",
                                      "13n" : "SnowNight",
                                      "50d" : "MistDay",
                                      "50n" : "MistNight"]
        
        return weatherImageDictionary[code] ?? ""
    }
}
