//
//  Forecast.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import Foundation
import ObjectMapper



struct Forecast : Mappable {
    
    var date : Date!
    private var strDate : String!
    var weatherDescription : String!
    var icon : String!
    var temperature : Float!
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        strDate              <- map["dt_txt"]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        date                 = formatter.date(from: strDate)
        weatherDescription   <- map["weather.0.description"]
        icon                 <- map["weather.0.icon"]
        temperature          <- map["main.temp"]
    }
    
}
