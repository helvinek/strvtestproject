//
//  CurrentWeatherViewModel.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

class CurrentWeatherViewModel {
    
    var currentWeather = PublishRelay<CurrentWeather>()
    var errorMessage = PublishRelay<String?>()
    
    private var imageIcon = PublishRelay<String>()
    lazy var weatherImage : Observable<UIImage> = currentWeather.map { UIImage(named : CurrentWeather.getImageNameWithCode($0.imageName)) ?? UIImage() }
    
    lazy var formattedLocation : Observable<String> = currentWeather.map {
        if let city = $0.cityName, let code = $0.countryCode {
            let country = self.countryNameWithCountryCode($0.countryCode) ?? ""
            return city + ", " + country
        } else {
            return String($0.latitude ?? 0) + " " + String($0.longitude ?? 0)
        }
    }
    
    lazy var formattedWeatherInfo : Observable<String> = currentWeather.map {
        let weather = $0.weatherDescription ?? ""
        return String(format:"%.1f°C", $0.temperature ?? 0) + " | " + weather.capitalized
    }
    lazy var humidity : Observable<String> = currentWeather.map {
        String(format: "%.1f%", $0.humidity ?? 0)
    }
    lazy var pressure : Observable<String> = currentWeather.map {
        String(format: "%.1f hPa", $0.pressure ?? 0)
    }
    lazy var precipitation : Observable<String> = currentWeather.map {
        String(format: "%.1f mm", $0.precipitation ?? 0)
    }
    lazy var windSpeed : Observable<String> = currentWeather.map {
        String(format: "%.1f km/h", $0.windSpeed ?? 0)
    }
    lazy var windDirection : Observable<String> = currentWeather.map {
        CurrentWeatherViewModel.getWindDirectionFromDegrees($0.windDegrees ?? 0)
    }
    
    private var disposeBag = DisposeBag()
    
    func refreshCurrentWeatherWithLocation(lat : String, lon : String){
        APIManager
            .shared
            .fetchCurrentWeatherObservable(latitude  : lat,
                                           longitude : lon)
            .subscribe(
                onNext: { [weak self] currentWeather in
                    if let self = self {
                        self.currentWeather.accept(currentWeather)
                        self.errorMessage.accept(nil)
                    }
                },
                onError: { [weak self] error in
                    if let self = self {
                        self.errorMessage.accept(error.localizedDescription)
                    }
            })
            .disposed(by: disposeBag)
        
    }
    
    static func getWindDirectionFromDegrees (_ degrees : Float) -> String{
        let value = Int((degrees/22.5) + 0.5)
        let directions = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
        return directions[(value%16)]
    }
    
    private func countryNameWithCountryCode(_ countryCode: String?) -> String? {
        guard let code = countryCode else {
            return ""
        }
        let locale = Locale(identifier: "en_US")
        return locale.localizedString(forRegionCode: code)
    }
    
}
