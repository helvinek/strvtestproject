//
//  ForecastViewModel.swift
//  STRVTestProject
//
//  Created by Rafael Montilla on 3/5/19.
//  Copyright © 2019 RMontilla. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation
import RxDataSources

struct SectionOfForecast {
    var header: String
    var items: [Item]
}
extension SectionOfForecast : SectionModelType {
    typealias Item = Forecast
    
    init(original: SectionOfForecast, items: [Item]) {
        self = original
        self.items = items
    }
}

class ForecastViewModel {
    
    var forecasts = PublishRelay<[Forecast]>()
    var errorMessage = PublishRelay<String?>()
    
    //Grouping forecasts by date
    lazy var orderedForecasts : Observable<[SectionOfForecast]> = forecasts.map {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let today = formatter.string(from: Date())
        let groupedDictionary = Dictionary(grouping: $0) { (forecast) -> String in
            let day = formatter.string(from: forecast.date ?? Date())
            return day == today ? today : day
        }
        let keys = groupedDictionary.keys.sorted { (dateStr1, dateStr2) -> Bool in
            return formatter.date(from: dateStr1)! < formatter.date(from: dateStr2)!
        }
        
        var forecastArray = [SectionOfForecast]()
        keys.forEach({ (key) in
            let sortedForecasts = groupedDictionary[key]!.sorted { (forecast1, forecast2) -> Bool in
                return forecast1.date < forecast2.date
            }
            let section = SectionOfForecast(header: key, items: sortedForecasts)
            forecastArray.append(section)
        })
        return forecastArray
        
    }
    
    lazy var formattedForecast : Observable<String> = orderedForecasts.map {
        
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        for dayForecast in $0 {
            
            let first = dayForecast.items[0]
            let day = dateFormatter.string(from: first.date)
            result += day + "\n"
            for timeForecast in dayForecast.items {
                let hour = timeFormatter.string(from: timeForecast.date)
                let weather = timeForecast.weatherDescription.capitalized
                let temperature = String(format: "%.2f", timeForecast.temperature)
                result += "\(hour) \(weather) \(temperature)°C\n"
            }
        }
        return result
    }
    
    private var disposeBag = DisposeBag()
    
    func refreshForecastWithLocation(lat : String, lon : String){
        
        APIManager
            .shared
            .fetchForecastObservable(latitude  : lat,
                                     longitude : lon)
            .subscribe(
                onNext: { [weak self] forecasts in
                    if let self = self {
                        self.forecasts.accept(forecasts)
                        self.errorMessage.accept(nil)
                    }
                },
                onError: { [weak self] error in
                    if let self = self {
                        self.errorMessage.accept(error.localizedDescription)
                    }
            })
            .disposed(by: disposeBag)
    }
    
}
